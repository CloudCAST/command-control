"use strict";

(function (window) {
    var SERVER = "https://cors-anywhere.herokuapp.com/http://demo.openhab.org:9080/rest/sitemaps/demo";

    // Use "widgets" for OpenHAB 2.x
    //var WIDGETS_PROPERTY = "widgets";
    // and "widget" for OpenHAB 1.x
    var WIDGETS_PROPERTY = "widget";

    // Error codes (mostly following Android error names and codes)
    var ERR_NETWORK = 2;
    var ERR_AUDIO = 3;
    var ERR_SERVER = 4;
    var ERR_CLIENT = 5;

    // Event codes
    var MSG_DOWNLOADING_SITEMAP = 1;
    var MSG_DOWNLOADED_SITEMAP = 2;
    var MSG_INIT_RECORDER = 3;
    var MSG_RECORDING = 4;
    var MSG_SEND = 5;
    var MSG_SEND_EMPTY = 6;
    var MSG_SEND_EOS = 7;
    var MSG_WEB_SOCKET = 8;
    var MSG_WEB_SOCKET_OPEN = 9;
    var MSG_WEB_SOCKET_CLOSE = 10;
    var MSG_STOP = 11;
    var MSG_SERVER_CHANGED = 12;

    var OpenHAB = function (cfg) {
        var config = cfg || {};
        config.server = config.server || SERVER;
        config.onInit = config.onInit || function () {
            };
        config.onEvent = config.onEvent || function (e, data) {
            };
        config.onError = config.onError || function (e, data) {
            };
        config.onNavigate = config.onNavigate || function () {
            };
        config.onActionChanged = config.onActionChanged || function (action) {
            };

        config.sitemap = null;

        // This is a list of path elements
        // Each path element contains:
        //  - Name: string to display in the breadcrumb widget
        //  - Keys: list is in order to allow jumping several levels at once (for frames or groups)
        config.currentState = [];

        // States
        this.NOT_INITED = 1;
        this.INITED = 2;

        this.state = this.NOT_INITED;

        // Returns the configuration
        this.getConfig = function () {
            return config;
        }

        this.setSitemap = function (sitemap) {
            this.sitemap = sitemap;
        }

        // Downloads the sitemap
        this.init = function () {
            config.onEvent(MSG_DOWNLOADING_SITEMAP, "Downloading sitemap ...");
            var request = $.ajax({
                type: "GET",
                url: config.server,
                data: {type: "json"}
            });

            request.done(function (data) {
                config.sitemap = data;
                config.onEvent(MSG_DOWNLOADED_SITEMAP, "Downloaded sitemap ...");

                config.onInit();
            });

            request.fail(function (jqXHR, textStatus) {
                config.onError(ERR_CLIENT, "Failed to download sitemap : " + textStatus);
            });

            this.state = this.INITED;
        }

        this.getState = function () {
            return config.currentState;
        }

        this.setState = function (state) {
            config.currentState = state;
            config.onNavigate();
        }

        function update_switch(link, button, data) {
            var command = "GET";
            var suffix = "/state"
            if (data) {
                command = "POST";
                suffix = "";
            }

            // Get the state of the switch
            var url = CORS_PROXY + link;
            button.button('loading');
            $.ajax({
                url: url + suffix,
                method: command,
                data: data,
                dataType: "text",
                contentType: "text/plain",
                context: {
                    $button: button,
                },
                success: function (state) {
                    console.dir(state);
                    if (data) {
                        update_switch(link, button);

                    } else {
                        if (state == "ON") {
                            this.$button.button('on');
                            this.$button.addClass('active');
                            this.$button.attr('aria-pressed', 'true');
                        } else {
                            this.$button.button('off');
                            this.$button.removeClass('active');
                            this.$button.attr('aria-pressed', 'false');
                        }

                        this.$button.data("state", state);
                    }
                },
                fail: function () {
                },
            });
        }


        this.performAction = function (action) {
            switch (action.type) {
                case "back":
                    config.currentState.pop();
                    config.onNavigate();
                    break;

                case "home":
                    config.currentState = [];
                    config.onNavigate();
                    break;

                case "navigation":
                    config.currentState.push(action.parameters.path);
                    config.onNavigate();
                    break;

                case "switch":
                    // TODO: implement a switch toggle

                    var current_state;
                    var next_state;

                    $.ajax({
                        method: "GET",
                        dataType: "text",
                        contentType: "text/plain",
                        url: action.parameters.widget.item.link + "/state",
                        success: function (result) {
                            current_state = result;
                        },
                        async: false
                    });

                    next_state =
                        (current_state == 'ON')     ? 'OFF'    :
                        (current_state == 'OFF')    ? 'ON'     :
                        (current_state == 'OPEN')   ? 'CLOSED' :
                        (current_state == 'CLOSED') ? 'OPEN'   :
                        (current_state == '0')      ? '100'    :
                        (current_state == '100')    ? '0'      : null ;

                    $.ajax({
                        url: action.parameters.widget.item.link,
                        method: "POST",
                        dataType: "text",
                        contentType: "text/plain",
                        data: next_state,
                        success: function (result) {
                            current_state = result;
                        },
                        async: false
                    });

                    config.onActionChanged(action);

                    break;

                default:
                    console.error("Action " + action.type + "not yet implmented.");
                    break;
            }
        }

        // Navigate to the current node of the sitemap and return the content
        this.getCurrentMap = function () {
            var map = config.sitemap.homepage;

            // Go to the current node
            for (var i = 0; i < config.currentState.length; i++) {
                var keys = config.currentState[i].keys;
                for (var j = 0; j < keys.length; j++)
                    map = map[keys[j]];
            }

            return map;
        }
    };

    window.OpenHAB = OpenHAB;

})(window);
